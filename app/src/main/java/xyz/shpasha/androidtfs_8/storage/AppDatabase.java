package xyz.shpasha.androidtfs_8.storage;


import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import xyz.shpasha.androidtfs_8.storage.model.Connection;
import xyz.shpasha.androidtfs_8.storage.model.Node;

@Database(entities = {Node.class, Connection.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    private static volatile AppDatabase INSTANCE;

    public abstract NodeDao nodeDao();

    public static AppDatabase getInstance(Context context) {
        if (INSTANCE == null) {
            synchronized (AppDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            AppDatabase.class, "my_db.db")
                            .build();
                }
            }
        }
        return INSTANCE;
    }
}