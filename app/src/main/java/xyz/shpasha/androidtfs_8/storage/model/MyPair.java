package xyz.shpasha.androidtfs_8.storage.model;

import android.arch.persistence.room.ColumnInfo;

public class MyPair {
    @ColumnInfo(name = "childCount")
    public Integer childCount;
    @ColumnInfo(name = "parentsCount")
    public Integer parentsCount;
}
