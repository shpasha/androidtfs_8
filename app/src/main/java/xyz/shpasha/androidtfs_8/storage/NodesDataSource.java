package xyz.shpasha.androidtfs_8.storage;

import java.util.List;

import io.reactivex.Flowable;
import xyz.shpasha.androidtfs_8.storage.model.Connection;
import xyz.shpasha.androidtfs_8.storage.model.MyPair;
import xyz.shpasha.androidtfs_8.storage.model.Node;

public interface NodesDataSource {
    Flowable<List<Node>> getNodes();
    Flowable<List<Node>> getChilds(long id);
    Flowable<List<Node>> getParents(long id);
    Flowable<MyPair> getChildsAndParentsCount(long id);
    Flowable<Integer> childsCount(long id);
    Flowable<Integer> parentsCount(long id);
    void deleteConnection(Connection connection);
    void insertOrUpdateNode(Node node);
    void addConnection(Connection connection);
}
