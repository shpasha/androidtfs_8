package xyz.shpasha.androidtfs_8.storage;


import android.arch.persistence.room.*;

import io.reactivex.Flowable;
import xyz.shpasha.androidtfs_8.storage.model.Connection;
import xyz.shpasha.androidtfs_8.storage.model.MyPair;
import xyz.shpasha.androidtfs_8.storage.model.Node;

import java.util.List;

@Dao
public abstract class NodeDao {

    @Query("SELECT * FROM node")
    public abstract Flowable<List<Node>> getAll();

    @Query("SELECT * FROM node WHERE id = :id")
    public abstract Node getById(int id);

    @Query("SELECT node.id, node.value FROM node " +
            "INNER JOIN connection " +
            "ON node.id = connection.childId " +
            "WHERE connection.parentId = :id")
    public abstract Flowable<List<Node>> getChilds(long id);

    @Query("SELECT node.id, node.value FROM node " +
            "INNER JOIN connection " +
            "ON node.id = connection.parentId " +
            "WHERE connection.childId = :id")
    public abstract Flowable<List<Node>> getParents(long id);

    @Query("SELECT count(*) FROM connection " +
            "WHERE parentId=:id")
    public abstract Flowable<Integer> childsCount(long id);

    @Query("SELECT count(*) FROM connection " +
            "WHERE childId=:id")
    public abstract Flowable<Integer> parentsCount(long id);

    @Query("SELECT " +
            "COUNT(CASE WHEN parentId=:id THEN 1 END) AS childCount, " +
            "COUNT(CASE WHEN childId=:id THEN 1 END) AS parentsCount " +
            "FROM connection")
    public abstract Flowable<MyPair> getChildsAndParentsCount(long id);

    @Query("DELETE FROM connection " +
            "WHERE parentId = :parentId " +
            "AND childId=:childId")
    public abstract void deleteConnection(Long parentId, Long childId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void addConnection(Connection connection);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void addNode(Node node);

    @Update
    public abstract void update(Node node);

    @Delete
    public abstract void delete(Node node);
}
