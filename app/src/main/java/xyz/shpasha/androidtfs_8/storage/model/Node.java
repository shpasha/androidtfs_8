package xyz.shpasha.androidtfs_8.storage.model;


import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class Node {
    @PrimaryKey(autoGenerate = true)
    public Long id;
    public Integer value;

    @Override
    public boolean equals(Object obj) {
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        return ((Node)obj).id == this.id;
    }
}
