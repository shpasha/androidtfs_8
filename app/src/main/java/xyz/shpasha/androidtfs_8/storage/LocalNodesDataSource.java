package xyz.shpasha.androidtfs_8.storage;

import java.util.List;

import io.reactivex.Flowable;
import xyz.shpasha.androidtfs_8.storage.model.Connection;
import xyz.shpasha.androidtfs_8.storage.model.MyPair;
import xyz.shpasha.androidtfs_8.storage.model.Node;

public class LocalNodesDataSource implements NodesDataSource {

    private final NodeDao nodeDao;

    public LocalNodesDataSource(NodeDao nodeDao) {
        this.nodeDao = nodeDao;
    }

    @Override
    public Flowable<List<Node>> getNodes() {
        return nodeDao.getAll();
    }

    @Override
    public Flowable<List<Node>> getChilds(long id) {
        return nodeDao.getChilds(id);
    }

    @Override
    public Flowable<List<Node>> getParents(long id) {
        return nodeDao.getParents(id);
    }

    @Override
    public Flowable<MyPair> getChildsAndParentsCount(long id) {
        return nodeDao.getChildsAndParentsCount(id);
    }


    @Override
    public Flowable<Integer> childsCount(long id) {
        return nodeDao.childsCount(id);
    }

    @Override
    public Flowable<Integer> parentsCount(long id) {
        return nodeDao.parentsCount(id);
    }

    @Override
    public void deleteConnection(Connection connection) {
        nodeDao.deleteConnection(connection.parentId, connection.childId);
    }

    @Override
    public void insertOrUpdateNode(Node node) {
        nodeDao.addNode(node);
    }

    @Override
    public void addConnection(Connection connection) {
        nodeDao.addConnection(connection);
    }
}
