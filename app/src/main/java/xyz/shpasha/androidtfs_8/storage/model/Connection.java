package xyz.shpasha.androidtfs_8.storage.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;
import xyz.shpasha.androidtfs_8.storage.model.Node;

@Entity
public class Connection {
    @PrimaryKey(autoGenerate = true)
    public long id;
    @ForeignKey(parentColumns = "id", entity = Node.class, childColumns = "childId")
    public long childId;
    @ForeignKey(parentColumns = "id", entity = Node.class, childColumns = "parentId")
    public long parentId;
}
