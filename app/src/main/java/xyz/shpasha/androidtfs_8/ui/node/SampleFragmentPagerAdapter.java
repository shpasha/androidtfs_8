package xyz.shpasha.androidtfs_8.ui.node;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import xyz.shpasha.androidtfs_8.ui.node.childs.ChildsFragment;
import xyz.shpasha.androidtfs_8.ui.node.parents.ParentsFragment;

public class SampleFragmentPagerAdapter extends FragmentPagerAdapter {
    private String tabTitles[] = new String[] { "Дети", "Родители" };
    private Context context;
    private long nodeId;

    public SampleFragmentPagerAdapter(FragmentManager fm, Context context, long nodeId) {
        super(fm);
        this.context = context;
        this.nodeId = nodeId;
    }

    @Override public int getCount() {
        return tabTitles.length;
    }

    @Override public Fragment getItem(int position) {

        return position == 0 ? ChildsFragment.newInstance(nodeId) : ParentsFragment.newInstance(nodeId);
    }

    @Override public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }
}