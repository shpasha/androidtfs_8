package xyz.shpasha.androidtfs_8.ui;

import android.arch.lifecycle.ViewModel;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import xyz.shpasha.androidtfs_8.storage.model.Connection;
import xyz.shpasha.androidtfs_8.storage.model.MyPair;
import xyz.shpasha.androidtfs_8.storage.model.Node;
import xyz.shpasha.androidtfs_8.storage.NodesDataSource;

public class NodesViewModel extends ViewModel {
    private final NodesDataSource nodesDataSource;

    public NodesViewModel(NodesDataSource nodesDataSource) {
        this.nodesDataSource = nodesDataSource;
    }

    public Flowable<List<Node>> getNodes() {
        return nodesDataSource.getNodes();
    }

    public Flowable<List<Node>> getChilds(long id) { return nodesDataSource.getChilds(id); }

    public Flowable<List<Node>> getParents(long id) { return nodesDataSource.getParents(id); }

    public Flowable<Integer> childsCount(long id) {return nodesDataSource.childsCount(id);}

    public Flowable<Integer> parentsCount(long id) {return nodesDataSource.parentsCount(id);}

    public Flowable<MyPair> getChildsAndParentsCount(long id) { return nodesDataSource.getChildsAndParentsCount(id); }


    public Completable deleteConnection(Connection connection) {
        return Completable.fromAction(() -> nodesDataSource.deleteConnection(connection));
    }

    public Completable addConnection(Connection connection) {
        return Completable.fromAction(() -> nodesDataSource.addConnection(connection));
    }

    public Completable addNode(Node node) {
        return Completable.fromAction(() -> nodesDataSource.insertOrUpdateNode(node));
    }


}
