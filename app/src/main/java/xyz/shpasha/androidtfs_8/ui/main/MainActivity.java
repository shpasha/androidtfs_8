package xyz.shpasha.androidtfs_8.ui.main;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import xyz.shpasha.androidtfs_8.*;
import xyz.shpasha.androidtfs_8.storage.AppDatabase;
import xyz.shpasha.androidtfs_8.storage.LocalNodesDataSource;
import xyz.shpasha.androidtfs_8.storage.model.Node;
import xyz.shpasha.androidtfs_8.ViewModelFactory;
import xyz.shpasha.androidtfs_8.ui.NodesViewModel;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements AddNodeFragment.AddNodeFragmentListener {

    private RecyclerView recyclerView;
    private MainRecyclerAdapter adapter;
    private List<Node> nodes;
    private FloatingActionButton fab;

    private NodesViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fab = findViewById(R.id.fab);
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        nodes = new ArrayList<>();
        adapter = new MainRecyclerAdapter(this, nodes);

        recyclerView.setAdapter(adapter);

        LocalNodesDataSource localNodesDataSource = new LocalNodesDataSource(AppDatabase.getInstance(this).nodeDao());
        viewModel = ViewModelProviders.of(this, new ViewModelFactory(localNodesDataSource)).get(NodesViewModel.class);

        AddNodeFragment addNodeFragment = new AddNodeFragment();
        fab.setOnClickListener(v -> addNodeFragment.show(getSupportFragmentManager(), "addNodeFragment"));

        viewModel.getNodes()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(data -> {
                    nodes.clear();
                    nodes.addAll(data);
                    adapter.notifyDataSetChanged();
                });

    }

    @Override
    public void onAdd(Integer val) {
        Node node = new Node();
        node.value = val;
        viewModel.addNode(node)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe();
    }
}
