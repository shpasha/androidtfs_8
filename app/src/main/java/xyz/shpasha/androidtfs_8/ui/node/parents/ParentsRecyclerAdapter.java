package xyz.shpasha.androidtfs_8.ui.node.parents;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import xyz.shpasha.androidtfs_8.*;
import xyz.shpasha.androidtfs_8.storage.*;
import xyz.shpasha.androidtfs_8.storage.model.Connection;
import xyz.shpasha.androidtfs_8.storage.model.Node;
import xyz.shpasha.androidtfs_8.ui.NodesViewModel;
import xyz.shpasha.androidtfs_8.ui.node.NodeActivity;

import java.util.ArrayList;
import java.util.List;

public class ParentsRecyclerAdapter extends RecyclerView.Adapter<ParentsRecyclerAdapter.MyViewHolder> {

    private Context context;
    private List<Node> parentNodes, allNodes, nodes;
    private NodesViewModel viewModel;
    private Long nodeId;

    private final int PARENT = 0;
    private final int OTHER = 1;

    public ParentsRecyclerAdapter(Context context, List<Node> parentNodes, List<Node> allNodes, Long nodeId) {
        this.context = context;
        this.parentNodes = parentNodes;
        this.allNodes = allNodes;
        this.nodeId = nodeId;
        makeNodes();
    }

    private void makeNodes() {
        if (nodes == null)
            nodes = new ArrayList<>();

        nodes.clear();
        nodes.addAll(parentNodes);

        for (Node n : allNodes) {
            if (!parentNodes.contains(n))
                nodes.add(n);
        }



        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ParentsRecyclerAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.recycler_item, viewGroup, false);
        return new ParentsRecyclerAdapter.MyViewHolder(view);
    }

    @Override
    public int getItemViewType(int position) {
        return position < parentNodes.size() ? PARENT : OTHER;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentsRecyclerAdapter.MyViewHolder viewHolder, int i) {
        Node node = nodes.get(i);
        viewHolder.nodeIdView.setText(node.id.toString());
        viewHolder.nodeValView.setText(node.value.toString());

        LocalNodesDataSource localNodesDataSource = new LocalNodesDataSource(AppDatabase.getInstance(context).nodeDao());
        viewModel = ViewModelProviders.of((NodeActivity)context, new ViewModelFactory(localNodesDataSource)).get(NodesViewModel.class);

        if (viewHolder.getItemViewType() == PARENT)
            viewHolder.container.setBackgroundColor(Color.GREEN);

        viewHolder.container.setOnClickListener(v -> {
            String dialogAction = viewHolder.getItemViewType() == PARENT ? "DELETE" : "ADD";
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
            alertDialog.setTitle("Action");
            alertDialog.setMessage("What?");
            alertDialog.setPositiveButton("CANCEL", (dialog, which) -> dialog.cancel());
            alertDialog.setNegativeButton(dialogAction, (dialog, which) -> {
                Connection connection = new Connection();
                connection.childId = nodeId;
                connection.parentId = node.id;
                if (viewHolder.getItemViewType() == PARENT) {
                    viewModel.deleteConnection(connection)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe();
                } else {
                    viewModel.addConnection(connection)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe();
                }
            });

            AlertDialog dialog = alertDialog.create();
            dialog.show();


        });

    }

    @Override
    public int getItemCount() {
        return nodes.size();
    }

    public void addParents(List<Node> data) {
        parentNodes = data;
        makeNodes();
    }

    public void addNodes(List<Node> data) {
        allNodes = data;
        makeNodes();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView nodeIdView, nodeValView;
        private ViewGroup container;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            nodeIdView = itemView.findViewById(R.id.idView);
            nodeValView = itemView.findViewById(R.id.valueView);
            container = itemView.findViewById(R.id.container);
        }
    }
}
