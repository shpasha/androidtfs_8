package xyz.shpasha.androidtfs_8.ui.node.parents;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import xyz.shpasha.androidtfs_8.*;
import xyz.shpasha.androidtfs_8.storage.AppDatabase;
import xyz.shpasha.androidtfs_8.storage.LocalNodesDataSource;
import xyz.shpasha.androidtfs_8.storage.model.Node;
import xyz.shpasha.androidtfs_8.ViewModelFactory;
import xyz.shpasha.androidtfs_8.ui.NodesViewModel;

import java.util.ArrayList;
import java.util.List;

public class ParentsFragment extends Fragment {
    private long nodeId;
    private ParentsRecyclerAdapter adapter;
    NodesViewModel viewModel;

    public static ParentsFragment newInstance(long nodeId) {
        Bundle args = new Bundle();
        args.putLong("id", nodeId);
        ParentsFragment fragment = new ParentsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            nodeId = getArguments().getLong("id");
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_page, container, false);
        RecyclerView recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        LocalNodesDataSource localNodesDataSource = new LocalNodesDataSource(AppDatabase.getInstance(getContext()).nodeDao());
        viewModel = ViewModelProviders.of(getActivity(), new ViewModelFactory(localNodesDataSource)).get(NodesViewModel.class);

        List<Node> childNodes = new ArrayList<>();
        List<Node> allNodes = new ArrayList<>();

        adapter = new ParentsRecyclerAdapter(getContext(), childNodes, allNodes, nodeId);

        viewModel.getParents(nodeId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(data -> {
                    adapter.addParents(data);
                });

        viewModel.getNodes()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(data -> {
                    adapter.addNodes(data);
                });

        recyclerView.setAdapter(adapter);




        return view;
    }
}
