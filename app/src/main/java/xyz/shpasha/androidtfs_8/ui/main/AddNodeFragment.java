package xyz.shpasha.androidtfs_8.ui.main;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import xyz.shpasha.androidtfs_8.R;

public class AddNodeFragment extends DialogFragment {
    private TextView nodeValView;

    public interface AddNodeFragmentListener {
        void onAdd(Integer val);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.add_node_dialog, null);

        nodeValView = view.findViewById(R.id.nodeValView);

        builder.setView(view)
                .setPositiveButton("Add", (dialog, id) -> {
                    ((AddNodeFragmentListener)getActivity()).onAdd(Integer.parseInt(nodeValView.getText().toString()));
                })
                .setNegativeButton("Cancel", (dialog, id) -> {
                });
        return builder.create();
    }

}
