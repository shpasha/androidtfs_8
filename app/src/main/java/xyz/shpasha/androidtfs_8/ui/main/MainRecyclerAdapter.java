package xyz.shpasha.androidtfs_8.ui.main;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import xyz.shpasha.androidtfs_8.*;
import xyz.shpasha.androidtfs_8.storage.AppDatabase;
import xyz.shpasha.androidtfs_8.storage.LocalNodesDataSource;
import xyz.shpasha.androidtfs_8.storage.model.Node;
import xyz.shpasha.androidtfs_8.ViewModelFactory;
import xyz.shpasha.androidtfs_8.ui.NodesViewModel;
import xyz.shpasha.androidtfs_8.ui.node.NodeActivity;

import java.util.List;

public class MainRecyclerAdapter extends RecyclerView.Adapter<MainRecyclerAdapter.MyViewHolder> {

    private Context context;
    private List<Node> nodes;
    private NodesViewModel viewModel;

    public MainRecyclerAdapter(Context context, List<Node> nodes) {
        this.context = context;
        this.nodes = nodes;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.recycler_item, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int i) {

        LocalNodesDataSource localNodesDataSource = new LocalNodesDataSource(AppDatabase.getInstance(context).nodeDao());
        viewModel = ViewModelProviders.of((MainActivity)context, new ViewModelFactory(localNodesDataSource)).get(NodesViewModel.class);

        Node node = nodes.get(i);
        viewHolder.nodeIdView.setText(node.id.toString());
        viewHolder.nodeValView.setText(node.value.toString());

        viewModel.getChildsAndParentsCount(node.id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(pair -> {
                   if (pair.childCount != 0 && pair.parentsCount != 0) {
                        viewHolder.container.setBackgroundColor(Color.RED);
                        return;
                   }
                   if (pair.childCount != 0) {
                       viewHolder.container.setBackgroundColor(Color.YELLOW);
                       return;
                   }
                    if (pair.parentsCount != 0) {
                        viewHolder.container.setBackgroundColor(Color.BLUE);
                        return;
                    }
                    viewHolder.container.setBackgroundColor(Color.TRANSPARENT);
                });

        viewHolder.container.setOnClickListener(v -> {
            Intent intent = new Intent(context, NodeActivity.class);
            intent.putExtra("id", node.id);
            context.startActivity(intent);
        });


    }

    @Override
    public int getItemCount() {
        return nodes.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView nodeIdView, nodeValView;
        private ViewGroup container;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            nodeIdView = itemView.findViewById(R.id.idView);
            nodeValView = itemView.findViewById(R.id.valueView);
            container = itemView.findViewById(R.id.container);
        }
    }
}
