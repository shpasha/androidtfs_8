package xyz.shpasha.androidtfs_8;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import xyz.shpasha.androidtfs_8.storage.NodesDataSource;
import xyz.shpasha.androidtfs_8.ui.NodesViewModel;

public class ViewModelFactory implements ViewModelProvider.Factory {

    private final NodesDataSource mDataSource;

    public ViewModelFactory(NodesDataSource dataSource) {
        mDataSource = dataSource;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        if (modelClass.isAssignableFrom(NodesViewModel.class)) {
            return (T) new NodesViewModel(mDataSource);
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }
}
